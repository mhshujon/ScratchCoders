<!--Footer-->
<footer class="page-footer center-on-small-only unique-color-dark">


    <!--Footer Links-->
    <div class="container mt-5 mb-4 text-center text-md-left">
        <div class="row">

            <!--First column-->
            <div class="col-md-3 col-lg-4 col-xl-3 mb-r">
                <h6 class="title font-bold">
                    <strong>RockON!</strong>
                </h6>
                <hr class="blue mb-4 pb-1 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
            <!--/.First column-->

            <!--Second column-->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-r">
                <h6 class="title font-bold">
                    <strong>Live Concert</strong>
                </h6>
                <hr class="blue mb-4 pb-1 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <a href="#!">RockON!</a>
                </p>
                <p>
                    <a href="#!">RockON!</a>
                </p>
                <p>
                    <a href="#!">RockON!</a>
                </p>
                <p>
                    <a href="#!">RockON!</a>
                </p>
            </div>
            <!--/.Second column-->

            <!--Third column-->
            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-r">
                <h6 class="title font-bold">
                    <strong>Useful links</strong>
                </h6>
                <hr class="blue mb-4 pb-1 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <a href="#!">RockON!</a>
                </p>
                <p>
                    <a href="#!">RockON!</a>
                </p>
                <p>
                    <a href="#!">RockON!</a>
                </p>
                <p>
                    <a href="#!">RockON!</a>
                </p>
            </div>
            <!--/.Third column-->

            <!--Fourth column-->
            <div class="col-md-4 col-lg-3 col-xl-3">
                <h6 class="title font-bold">
                    <strong>Contact</strong>
                </h6>
                <hr class="blue mb-4 pb-1 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p>
                    <i class="fa fa-home mr-3"></i> RockON!</p>
                <p>
                    <i class="fa fa-envelope mr-3"></i> info@RockON!.com</p>
                <p>
                    <i class="fa fa-phone mr-3"></i> + 01 234 567 88</p>
                <p>
                    <i class="fa fa-print mr-3"></i> + 01 234 567 89</p>
            </div>
            <!--/.Fourth column-->

        </div>
    </div>
    <!--/.Footer Links-->

    <!-- Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">
            © 2018 Copyright:
            <a href="#!">
                <strong> ScratchCoders</strong>
            </a>
        </div>
    </div>
    <!--/.Copyright -->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>

<!-- Bootstrap dropdown -->
<script type="text/javascript" src="js/popper.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<!-- Custom JS -->
<script type="text/javascript" src="js/mdb.min.js"></script>

<script>
    new WOW().init();
</script>

</body>

</html>